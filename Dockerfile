FROM rocker/shiny:3.6.1



# Copy configuration files into the Docker image
COPY shiny-server.conf  /etc/shiny-server/shiny-server.conf
COPY /app /srv/shiny-server/

# make all files readable
RUN chmod -R 755 /srv/shiny-server/

# Copy further configuration files into the Docker image
#COPY shiny-server.sh /usr/bin/shiny-server.sh

# Install R packages that are required
# TODO: add further package if you need!
RUN R -e "install.packages(c('shiny', 'shinydashboard','DT', 'ggplot2', 'ggrepel', 'gridExtra','grid','reshape2','rintrojs','tidyr'), repos='http://cran.rstudio.com/')"
